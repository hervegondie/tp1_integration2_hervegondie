// const image1 = "./images/digital-marketing.jpg"
// const image2 = "./images/bureau_projet.jpg"

const image1 = document.getElementById('image1')
const image2 = document.getElementById('image2')

const boutonPrecedent = document.getElementById('boutonprecedent')
const boutonSuivant = document.getElementById('boutonsuivant')

boutonSuivant.addEventListener('click', cliqueSuivant)
boutonPrecedent.addEventListener('click', cliquePrecedent)

function cliqueSuivant(){
    image1.style.display = 'none'
    image2.style.display = 'flex'
}

function cliquePrecedent(e){
    image2.style.display = 'none'
    image1.style.display = 'flex'
}

